/*
 * ADC.h
 *
 *  Created on: 24.02.2022
 *      Author: ebuss
 */

#ifndef INC_ADC_H_
#define INC_ADC_H_


/* USER CODE BEGIN EFP */
void InitADC();
int32_t  BioPotential();
uint16_t readBio();
/* USER CODE END EFP */
#endif /* INC_ADC_H_ */
