/*
 * ADC.c
 *
 *  Created on: 24.02.2022
 *      Author: ebuss
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32_seq.h"
/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
static const uint8_t SLAVE_ADDR 				= 0x40;
static const uint8_t RESET_COMMAND[1] 			= {0x06};
uint8_t READ_COMMAND[1] 			= {0x10};

static const uint32_t databits 					= 16777216;  //2^24
static const float Vref 						= 2.048;		//
static const uint8_t Gain 						= 4;
static const float OneCode 						= ((2*Vref / Gain) ) /databits;
static const uint8_t NumCalibrationLoops 		= 100;
static const uint8_t Reg0_w 					= 0x40;
static const uint8_t Reg1_w 					= 0x44;
static const uint8_t Reg2_w 					= 0x48;
static const uint8_t Reg3_w 					= 0x4C;



/* USER CODE END PD */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;
UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
int32_t Biopot = 0;
uint8_t counter = 0;
int32_t Biotest = 0;


uint8_t calibrate = 1;
double calibrationvalues[100];
float offset = 0;

HAL_StatusTypeDef ret0;
HAL_StatusTypeDef ret1;
HAL_StatusTypeDef ret2;
char buf[30];

/* USER CODE END PV */


/* USER CODE BEGIN PFP */
void ResetADC(){
	ret0 = HAL_I2C_Master_Transmit(&hi2c1, (SLAVE_ADDR<< 1), RESET_COMMAND, 2,500);  // size = 1
	if (ret0 != HAL_OK) {
		  strcpy((char*)buf,"Error while reseting!\r\n");
		} else {
		  strcpy((char*)buf," ADS reseted!\r\n");
		}
	HAL_UART_Transmit(&huart1, buf, strlen((char*)buf), HAL_MAX_DELAY);
	HAL_Delay(5);
}

void SetRegister(){
	if (calibrate == 1){
		uint8_t pin_reg[2] = {Reg0_w,0xE0};		// set adc input pins
		 ret0 = HAL_I2C_Master_Transmit(&hi2c1, (SLAVE_ADDR<< 1), pin_reg, 2,500);
	} else {
		//uint8_t pin_reg[2] = {Reg0_w,0x60};
		uint8_t pin_reg[2] = {Reg0_w,0x04};
		ret2 = HAL_I2C_Master_Transmit(&hi2c1, (SLAVE_ADDR<< 1), pin_reg, 2,500);
	}

	uint8_t mode[2] = {Reg1_w,0x8}; 			//continous mode
	ret1 = HAL_I2C_Master_Transmit(&hi2c1, (SLAVE_ADDR<< 1), mode, 2,500);

	//uint8_t drdy_reg[2] = {Reg2_w,0x80};			// drdy_pin if new conversation is available
	//ret1 = HAL_I2C_Master_Transmit(&hi2c1, (SLAVE_ADDR<< 1), drdy_reg, 2,500);
	//uint8_t dataCounter_reg[2] = {Reg2_w,0x40};			// drdy_pin if new conversation is available
	//ret1 = HAL_I2C_Master_Transmit(&hi2c1, (SLAVE_ADDR<< 1), dataCounter_reg, 2,500);

	if (ret0 != HAL_OK || ret1 != HAL_OK) {
		strcpy((char*)buf,"Error Register!\r\n");
	} else {
		strcpy((char*)buf,"Register set!\r\n");
	}

	HAL_UART_Transmit(&huart1, buf, strlen((char*)buf), HAL_MAX_DELAY);
	HAL_Delay(6);
}


void Calibration(){

	if (counter < NumCalibrationLoops){
		ret0 = HAL_I2C_Master_Transmit(&hi2c1, (SLAVE_ADDR<< 1),READ_COMMAND, 1,500);  // size = 1

		uint8_t measurement_byte [3] = {0,0,0};
		ret1 = HAL_I2C_Master_Receive(&hi2c1, ((SLAVE_ADDR << 1) | 0x01), &measurement_byte, 3, 50);

		if (ret1 != HAL_OK) {
			strcpy((char*)buf,"got data failed\r\n");
		} else {
			strcpy((char*)buf," got data\r\n");
		}
		long measurment = (( ( (long) measurement_byte[0]) << 24) | (long ) (measurement_byte[1] << 16) | (long ) (measurement_byte[2]<< 8)) >>8; //pushing due to 32bit-24bit SIGNED!
		//if(measurment & 0x800000){ //inverse 2nd complement
		//measurment |= 0xFF000000;
		//  }
		float AnalogValue = measurment*OneCode;
		float test = AnalogValue;
		//HAL_UART_Transmit(&huart1, (uint8_t*)buf, sprintf(buf, "%d %d %d %d  \r\n", measurement_byte[3],measurement_byte[2],measurement_byte[1],measurement_byte[0]), HAL_MAX_DELAY);
		//HAL_UART_Transmit(&huart1, (uint8_t*)buf, sprintf(buf, " %g %g \r\n", AnalogValue, test), HAL_MAX_DELAY);
		//HAL_Delay(500);
		calibrationvalues[counter] = AnalogValue;
		counter = counter +1;


		//HAL_Delay(5);


	} else {
		counter = 0;
		float sumoffset = 0;
		for(int i=0; i<NumCalibrationLoops; i++) {
			sumoffset += calibrationvalues[i];
			//HAL_UART_Transmit(&huart1, (uint8_t*)buffer, sprintf(buffer, " %f %f %f \r\n", calibrationvalues[i],sumoffset, sumoffset/(i+1)), HAL_MAX_DELAY);

		}
		offset = sumoffset/NumCalibrationLoops;
		HAL_UART_Transmit(&huart1, (uint8_t*)buf, sprintf(buf, "\n Calibration offset %g \r\n", offset), HAL_MAX_DELAY);


		calibrate = 0;

		uint8_t pin_reg[2] = {Reg0_w,0x04}; // start reading
		ret2 = HAL_I2C_Master_Transmit(&hi2c1, (SLAVE_ADDR<< 1), pin_reg, 2,500);
		UTIL_SEQ_RegTask( 1<<CFG_DRDY_INT, UTIL_SEQ_RFU, readBio);
	}

}


uint16_t readBio(){
	uint16_t BioPotential = 0;
	ret0 = HAL_I2C_Master_Transmit(&hi2c1, (SLAVE_ADDR<< 1),READ_COMMAND, 1,500);  // size = 1

	u_int8_t measurement_byte [3] = {0,0,0};
	ret1 = HAL_I2C_Master_Receive(&hi2c1, ((SLAVE_ADDR << 1) | 0x01), measurement_byte, 3, 50);
	int32_t b0 = 0;
	//uint32_t b1 = 0;
	//u_int16_t b2 = 0;
	b0 = measurement_byte[0] << 24;
	//b1= measurement_byte[1] << 16;
	//b2 = measurement_byte[2]<< 8;
	long measurment = (( ( (long) measurement_byte[0]) << 24) | (long ) (measurement_byte[1] << 16) | (long ) (measurement_byte[2]<< 8)) >>8; //pushing due to 32bit-24bit SIGNED!
	//int32_t measurment = (b0|b1|b2)>>8;
	//if(measurment &   0x800000){
	//	measurment |= 0xFF000000;
		//measurment = measurment <<8;
		//measurment = measurment >>8;
	//}
	float AnalogValue = measurment*OneCode - offset; //+0.43;//-0.014;
	float mBio =  AnalogValue * 1e+3;
	float mmBio =  AnalogValue * 1e+6;
	float nBio =  AnalogValue * 1e+9;
	Biopot = nBio;
	//HAL_UART_Transmit(&huart1, (uint8_t*)buffer, sprintf(buffer, " hallo %f \r\n", realValue), HAL_MAX_DELAY);
	//HAL_UART_Transmit(&huart1, (uint8_t*)buffer, sprintf(buffer, "%d %d %d %d %f %f  \r\n", clear_value,clear_data[0],clear_data[1],clear_data[2], realValue, realValue+ 0x800000), HAL_MAX_DELAY);
	HAL_UART_Transmit(&huart1, (uint8_t*)buf, sprintf(buf, " %f %f %f %f %d \r\n", AnalogValue,mBio, mmBio, nBio,Biopot), HAL_MAX_DELAY);

	//HAL_UART_Transmit(&huart1, (int*)clear_value, 2, HAL_MAX_DELAY);
	//HAL_Delay(5);
	//ret0 = HAL_I2C_Master_Transmit(&hi2c1, (SLAVE_ADDR<< 1),READ_COMMAND, 1,500);  // size = 1

	return BioPotential;
}
int32_t BioPotential(){
return Biopot;
}

void InitADC(I2C_HandleTypeDef hi2c1_m, UART_HandleTypeDef huart1_m){
	hi2c1 = hi2c1_m;
	huart1 = huart1_m;
	if(calibrate == 1){
		UTIL_SEQ_RegTask( 1<<CFG_DRDY_INT, UTIL_SEQ_RFU, Calibration);
	} else {
		UTIL_SEQ_RegTask( 1<<CFG_DRDY_INT, UTIL_SEQ_RFU, readBio);
	}

		HAL_StatusTypeDef ret0;
		HAL_StatusTypeDef ret1;
		  	ret1 = HAL_I2C_IsDeviceReady(&hi2c1, (SLAVE_ADDR << 1), 10, 500);
		  if(ret1 == HAL_OK){			// checks if i2c is ready
			  HAL_GPIO_WritePin(LD1_GPIO_Port, LD1_Pin, 1);
		  }

		ResetADC();
		SetRegister();

		char Start[1] = {0x08}; // send only first adress
			ret0 = HAL_I2C_Master_Transmit(&hi2c1, (SLAVE_ADDR<< 1),Start, 1,500);  // size = 1
			if (ret0 != HAL_OK) {
				  strcpy((char*)buf," start command failed!\r\n");
				} else {
				  strcpy((char*)buf," start command, waitinf of drdy\r\n");
				}
			HAL_UART_Transmit(&huart1, buf, strlen((char*)buf), HAL_MAX_DELAY);
 			HAL_Delay(500);
}
