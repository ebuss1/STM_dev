/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    App/custom_app.c
  * @author  MCD Application Team
  * @brief   Custom Example Application (Server)
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "app_common.h"
#include "dbg_trace.h"
#include "ble.h"
#include "custom_app.h"
#include "custom_stm.h"
#include "stm32_seq.h"
#include "app_ble.h"
#include "ADC.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef struct
{
	uint16_t Value;
} DataToSend_t;
typedef struct
{
  /* MyPeripheral */
  uint8_t               Mychar_Notification_Status;
  uint8_t               Notificatn_allwed_bywriteChar;
  uint16_t              ConnectionHandle;
  DataToSend_t 			Data;
  uint8_t				Update_timer_Id;
} Custom_App_Context_t;

/* USER CODE BEGIN PTD */
#define Data_CHANGE_PERIOD (0.1*1000*1000/CFG_TS_TICK_VAL) /*100 ms/*
/* USER CODE END PTD */

/* Private defines ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macros -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/**
 * START of Section BLE_APP_CONTEXT
 */

PLACE_IN_SECTION("BLE_APP_CONTEXT") static Custom_App_Context_t Custom_App_Context;

/**
 * END of Section BLE_APP_CONTEXT
 */

/* USER CODE BEGIN PV */
uint8_t UpdateCharData[247];
uint8_t NotifyCharData[247];

uint8_t SecureReadData;
uint8_t ascendingNo = 0;
int32_t newval = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
  /* MyPeripheral */
static void Custom_Mychar_Update_Char(void);
static void Custom_Mychar_Send_Notification(void);
static void DataChange_Timer_Callback(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Functions Definition ------------------------------------------------------*/
void Custom_STM_App_Notification(Custom_STM_App_Notification_evt_t *pNotification)
{
  /* USER CODE BEGIN CUSTOM_STM_App_Notification_1 */

  /* USER CODE END CUSTOM_STM_App_Notification_1 */
  switch(pNotification->Custom_Evt_Opcode)
  {
    /* USER CODE BEGIN CUSTOM_STM_App_Notification_Custom_Evt_Opcode */
  	  PRINT_MESG_DBG("In Custom_STM_App_Notification\n");
    /* USER CODE END CUSTOM_STM_App_Notification_Custom_Evt_Opcode */

  /* MyPeripheral */
    case CUSTOM_STM_MYCHAR_READ_EVT:
      /* USER CODE BEGIN CUSTOM_STM_MYCHAR_READ_EVT */
    	PRINT_MESG_DBG("ACI_GATT_ATTRIBUTE_MODIFIED_VSEVT_CODE My_Char Read\n");
      /* USER CODE END CUSTOM_STM_MYCHAR_READ_EVT */
      break;

    case CUSTOM_STM_MYCHAR_NOTIFY_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_MYCHAR_NOTIFY_ENABLED_EVT */
    	Custom_App_Context.Mychar_Notification_Status = 1;
    	PRINT_MESG_DBG("ACI_GATT_ATTRIBUTE_MODIFIED_VSEVT_CODE My_Char notify\n");
    	if (Custom_App_Context.Notificatn_allwed_bywriteChar) {
    		HW_TS_Start(Custom_App_Context.Update_timer_Id, Data_CHANGE_PERIOD);
    	} else {
    		PRINT_MESG_DBG("Communication not allwed, set it with write char\n");
    	}

      /* USER CODE END CUSTOM_STM_MYCHAR_NOTIFY_ENABLED_EVT */
      break;

    case CUSTOM_STM_MYCHAR_NOTIFY_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_MYCHAR_NOTIFY_DISABLED_EVT */
    	Custom_App_Context.Mychar_Notification_Status = 0;
    	HW_TS_Stop(Custom_App_Context.Update_timer_Id);
      /* USER CODE END CUSTOM_STM_MYCHAR_NOTIFY_DISABLED_EVT */
      break;

    case CUSTOM_STM_MYCHAR_WRITE_PERMIT_REQ_VSEVT_CODE:
    	/* USER CODE BEGIN CUSTOM_STM_HRS_CTRLP_WRITE_EVT */
    	   PRINT_MESG_DBG("ACI_GATT_WRITE_PERMIT_REQ_VSEVT_CODE My_HRS_CTRL_Point Write\n");
    	   PRINT_MESG_DBG("Initialize reading\n");
    	   PRINT_MESG_DBG("sensorfreq %d: \n", sensor_freq);
    	   Custom_App_Context.Notificatn_allwed_bywriteChar = 1;
    	   //Ble_Hci_Gap_Gatt_Init_test();
    	 /* reset energy expended */
    	 //hr_energy_reset = CUSTOM_STM_HRS_ENERGY_RESET;
    	 /* USER CODE END CUSTOM_STM_HRS_CTRLP_WRITE_EVT */
         break;
    default:
      /* USER CODE BEGIN CUSTOM_STM_App_Notification_default */

      /* USER CODE END CUSTOM_STM_App_Notification_default */
      break;
  }
  /* USER CODE BEGIN CUSTOM_STM_App_Notification_2 */

  /* USER CODE END CUSTOM_STM_App_Notification_2 */
  return;
}

void Custom_APP_Notification(Custom_App_ConnHandle_Not_evt_t *pNotification)
{
  /* USER CODE BEGIN CUSTOM_APP_Notification_1 */

  /* USER CODE END CUSTOM_APP_Notification_1 */

  switch(pNotification->Custom_Evt_Opcode)
  {
    /* USER CODE BEGIN CUSTOM_APP_Notification_Custom_Evt_Opcode */

    /* USER CODE END P2PS_CUSTOM_Notification_Custom_Evt_Opcode */
    case CUSTOM_CONN_HANDLE_EVT :
      /* USER CODE BEGIN CUSTOM_CONN_HANDLE_EVT */

      /* USER CODE END CUSTOM_CONN_HANDLE_EVT */
      break;

    case CUSTOM_DISCON_HANDLE_EVT :
      /* USER CODE BEGIN CUSTOM_DISCON_HANDLE_EVT */

      /* USER CODE END CUSTOM_DISCON_HANDLE_EVT */
      break;

    default:
      /* USER CODE BEGIN CUSTOM_APP_Notification_default */

      /* USER CODE END CUSTOM_APP_Notification_default */
      break;
  }

  /* USER CODE BEGIN CUSTOM_APP_Notification_2 */

  /* USER CODE END CUSTOM_APP_Notification_2 */

  return;
}

void Custom_APP_Init(void)
{
  /* USER CODE BEGIN CUSTOM_APP_Init */
	Custom_App_Context.Data.Value = 0;
	Custom_App_Context.Notificatn_allwed_bywriteChar = 1;
	UTIL_SEQ_RegTask( 1<<CFG_TASK_NOTIFYDATA_ID, UTIL_SEQ_RFU, Custom_Mychar_Send_Notification);
	/* Create timer to change the Temperature and update charecteristic */
	HW_TS_Create(CFG_TIM_PROC_ID_ISR,&(Custom_App_Context.Update_timer_Id),hw_ts_Repeated,DataChange_Timer_Callback);

  /* USER CODE END CUSTOM_APP_Init */
  return;
}

/* USER CODE BEGIN FD */

/* USER CODE END FD */

/*************************************************************
 *
 * LOCAL FUNCTIONS
 *
 *************************************************************/
static void DataChange_Timer_Callback(void)
{
	UTIL_SEQ_SetTask(1<<CFG_TASK_NOTIFYDATA_ID, CFG_SCH_PRIO_0);
}

  /* MyPeripheral */
void Custom_Mychar_Update_Char(void) /* Property Read */
{
  Custom_STM_App_Update_Char(CUSTOM_STM_MyNOTIFY_CHAR, (uint8_t *)UpdateCharData);
  /* USER CODE BEGIN Mychar_UC*/

  /* USER CODE END Mychar_UC*/
  return;
}

void Custom_Mychar_Send_Notification(void) /* Property Notification */
 {
  if(Custom_App_Context.Mychar_Notification_Status)
  {
	newval = BioPotential();
	APP_DBG_MSG("bio %d ",newval);
	//uint8_t randomNo = rand() % 256;
	//NotifyCharData[0] = randomNo;
	NotifyCharData[3] = newval & 0xff;
	NotifyCharData[2] = (newval >> 8) & 0xff;
	NotifyCharData[1] = (newval >> 16) & 0xff;
	NotifyCharData[0] = (newval >> 24);
    Custom_STM_App_Update_Char(CUSTOM_STM_MyNOTIFY_CHAR, (uint8_t *)NotifyCharData);
    /* USER CODE BEGIN Mychar_NS*/

    /* USER CODE END Mychar_NS*/
  }
  else
  {
    APP_DBG_MSG("-- CUSTOM APPLICATION : CAN'T INFORM CLIENT -  NOTIFICATION DISABLED\n ");
  }
  return;
}

/* USER CODE BEGIN FD_LOCAL_FUNCTIONS*/

/* USER CODE END FD_LOCAL_FUNCTIONS*/
